import {AbstractControl, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';


export class FormValidators  {

  constructor() { }

  static onlyLetters(control: AbstractControl): ValidationErrors | null {
    const regex = new RegExp('^[a-zA-Z|ñ|Ñ ]+$');
    const allow = regex.test(control.value);

    return allow ?  null : {onlyLetters: {value: control.value}};
  }

}
