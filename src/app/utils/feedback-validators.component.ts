import {Component, Input} from '@angular/core';

@Component({
  styles : ['.invalid-feedback{display: inline-block;font-size: 80%;color: #dc3545;}'],
  selector : 'app-feedback-validator',
  template :
    `<div *ngIf="submitted && errors" class="invalid-feedback">
      <div *ngIf="errors.required">El campo {{attr}} es requerido.</div>
      <div *ngIf="errors.min">El {{attr}} debe ser mayor que {{errors.min.min}}.</div>
      <div *ngIf="errors.size">El campo {{attr}} debe tener {{errors.size.requiredSize}} caracteres.</div>
      <div *ngIf="errors.numeric">El campo {{attr}} debe ser numérico.</div>
      <div *ngIf="errors.email">El campo {{attr}} debe ser un correo electrónico válido.</div>
      <div *ngIf="errors.max">El número máximo admitido para el campo {{attr}} es {{errors.max.max}}.</div>
      <div *ngIf="errors.minLength">El número mínimo de caracteres para el campo {{attr}} es {{errors.min.length}}.</div>
      <div *ngIf="errors.maxLength">El número máximo de caracteres para el campo {{attr}} es {{errors.ma.length}}.</div>
      <div *ngIf="errors.onlyLetters">El campo {{attr}} debe de estar compuesto unicamente por letras.</div>
      <!--<pre>{{errors | json}}</pre>-->
    </div>`
})

export class FeedbackValidatorsComponent {

  @Input() submitted;
  @Input() errors;
  @Input() attr = 'Field';
  constructor() {}

}
