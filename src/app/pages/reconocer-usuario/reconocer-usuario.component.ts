import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { UsuariotecService } from 'src/app/api/usuariotec.service';
import { UserTecnm } from 'src/app/domain/user-tecnm';
import { WebcamImage } from 'src/app/domain/webcam/webcam-image';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reconocer-usuario',
  templateUrl: './reconocer-usuario.component.html',
  styleUrls: ['./reconocer-usuario.component.css']
})
export class ReconocerUsuarioComponent implements OnInit {
  
  triggerCam: Subject<void> = new Subject<void>();
  checkMiembroTec = true;
  form: FormGroup;
  submitted = false;
  snEntrada = true;

  constructor(
    private fb: FormBuilder,
    private faceRegService: UsuariotecService,
    private router: Router
  ) { 
    this.form = this.fb.group({      
      numControl: [null, [Validators.required]],      
    });
  }

  ngOnInit(): void {
  }

  public tomarRostro(snEntrada){
    this.snEntrada = snEntrada;
    this.triggerCam.next();
  }

  public capturarRostro(image: WebcamImage){
    this.submitted = true;    
    if (this.form.invalid) { return; }

    const dataForm = this.form.value;
    dataForm.base64Image = image.imageAsDataUrl;
    dataForm.snEntrada = this.snEntrada;

    console.log(dataForm)    

    if(this.checkMiembroTec){
      this.faceRegService.obtenerUsuarioPorNumControl(dataForm.numControl).subscribe(res=>{
        dataForm.id = res.data.id;        

        this.reconocimientoUsuario(dataForm);
      });
    }else{      
      this.reconocimientoUsuario(dataForm);
    }    
  }

  get formControls(){
    return this.form.controls;
  }

  private reconocimientoUsuario(dataImage){
    this.faceRegService.reconocimientoUsuario(dataImage).subscribe(res =>{

      const labelAcceso = dataImage.snEntrada ? 'entrada':'salida';      

      this.faceRegService.obtenerUsuarioPorID(dataImage.id).subscribe(res=>{        

        const dataAcceso = res.data;

        Swal.fire({
          icon: 'success',
          title: `Se ha registrado correctamente el acceso de ${labelAcceso}`,
          text: `El usuario ${dataAcceso.nombre} ${dataAcceso.apellidos} ha registrado correctamente el acceso.`
        });

        this.router.navigateByUrl('/tecnm');
      });
      
    });
  }

  changeCheckCambio(event: any){
    this.checkMiembroTec = !this.checkMiembroTec;

    if (this.checkMiembroTec){
      this.form.addControl('numControl', new FormControl(null, Validators.required));
      this.form.removeControl('id');
    }else{
      this.form.addControl('id', new FormControl(null, Validators.required));
      this.form.removeControl('numControl');
    }
  }

}