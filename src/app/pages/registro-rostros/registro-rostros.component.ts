import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { UsuariotecService } from 'src/app/api/usuariotec.service';
import { ModalImagesComponent } from 'src/app/components/modal-images/modal-images.component';
import { UserTecnm } from 'src/app/domain/user-tecnm';
import { WebcamImage } from 'src/app/domain/webcam/webcam-image';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-rostros',
  templateUrl: './registro-rostros.component.html',
  styleUrls: ['./registro-rostros.component.css']
})
export class RegistroRostrosComponent implements OnInit {

  triggerCam: Subject<void> = new Subject<void>();
  checkMiembroTec = true;
  bsModalRef: BsModalRef;
  userData: UserTecnm;
  idUser: string

  constructor(
    private fb: FormBuilder,
    private faceRegService : UsuariotecService,
    private route: ActivatedRoute,
    private modalService: BsModalService,
    private router: Router
  ) {     
    this.idUser = this.route.snapshot.params.id;

    this.faceRegService.obtenerUsuarioPorID(this.idUser).subscribe(res=>{
      if(res.data == null){

        Swal.fire({
          icon: 'error',
          title: 'No existe el usuario con el id indicado.',
          text: "Favor de comunicarse con el administrador del sistema"
        });

        this.router.navigateByUrl('/tecnm');
      }

      this.userData = res.data;      
    });

  }

  ngOnInit(): void {
  }

  public tomarRostro(){
    this.triggerCam.next();
  }

  public capturarRostro(image: WebcamImage){
    console.log(image);

    const dataBody = {
      numControl: this.userData.numControl,
      id: this.userData.id,
      base64Image: image.imageAsDataUrl
    };

    this.faceRegService.registrarRostro(dataBody).subscribe(res=>{

      const initialState = {
        image: res.data.image
      };
  
      this.bsModalRef = this.modalService.show(
        ModalImagesComponent, {initialState, class: 'modal-extra-lg'});
      this.bsModalRef.content.close = () => {
      }
    });    
  }

}
