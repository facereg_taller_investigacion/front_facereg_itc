import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuariotecService } from 'src/app/api/usuariotec.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {

  form: FormGroup;
  checkMiembroTec = false;
  submitted = false;
  
  constructor(
    private fb: FormBuilder,
    private faceRegService: UsuariotecService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({      
      nombre: [null, [Validators.required]],
      apellidos: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],      
    });
  }

  public registrar(){
    this.submitted = true;
    console.log('register')
    console.log(this.form.controls)
    if (this.form.invalid) { return; }
    
    const dataForm = this.form.value;    

    dataForm.snVisitante = false

    if(!this.checkMiembroTec){
      dataForm.numControl = 0;
      dataForm.snVisitante = true
    }
    
    this.faceRegService.registrarUsuario(dataForm).subscribe(res =>{
      Swal.fire({
        icon: 'success',
        title: 'Usuario creado exitosamente.',
        text: `El usuario con ID ${res.data.id} ha sido creado exitosamente`
      });

      this.router.navigateByUrl(`tecnm/registro/${res.data.id}`)
    });

  }

  get formControls(){
    return this.form.controls;
  }

  changeCheckCambio(event: any){
    this.checkMiembroTec = !this.checkMiembroTec;

    if (this.checkMiembroTec){
      this.form.addControl('numControl', new FormControl(null, Validators.required));
    }else{
      this.form.removeControl('numControl');
    }
  }

}
