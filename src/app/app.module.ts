import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgHttpLoaderModule} from 'ng-http-loader';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UrlInterceptorService} from './network/url-interceptor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import { ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {FeedbackValidatorsComponent} from './utils/feedback-validators.component';
import {DatePipe} from '@angular/common';
import {ModalModule} from 'ngx-bootstrap/modal';
import { HomeComponent } from './pages/home/home.component';
import { WebcamModule } from './modules/webcam.module';
import { WebcamFaceregComponent } from './components/webcam-facereg/webcam-facereg.component';
import { RegistroUsuarioComponent } from './pages/registro-usuario/registro-usuario.component';
import { RegistroRostrosComponent } from './pages/registro-rostros/registro-rostros.component';
import { ReconocerUsuarioComponent } from './pages/reconocer-usuario/reconocer-usuario.component';
import { ModalImagesComponent } from './components/modal-images/modal-images.component';

@NgModule({
  declarations: [
    AppComponent,    
    FeedbackValidatorsComponent,    
    HomeComponent, WebcamFaceregComponent,
     RegistroUsuarioComponent, 
     RegistroRostrosComponent, ReconocerUsuarioComponent, ModalImagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgHttpLoaderModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgxMaskModule.forRoot(),
    WebcamModule
  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
