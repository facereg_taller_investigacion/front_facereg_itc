import { Component } from '@angular/core';
import {UrlInterceptorService} from './network/url-interceptor.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ProyectoCoppel';

  constructor(
    urlInterceptor: UrlInterceptorService,
  ) {
    urlInterceptor.error.subscribe(res => {
      console.log(res)
      Swal.fire({
        icon: 'warning',
        title: 'Advertencia',
        text: res.msg.body
      });
    });

  }
}
