import { Injectable, Output, EventEmitter} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {catchError} from 'rxjs/internal/operators';
import {ErrorHandlerService} from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class UrlInterceptorService implements HttpInterceptor{

  @Output() error: EventEmitter<any> = new EventEmitter(true);

  constructor( private errorHandler: ErrorHandlerService) {
    errorHandler.errorEvent.subscribe(res => {
      this.error.emit(res);
    });
   }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('https://')
      || req.url.includes('http://')
    ){
      return next.handle(req.clone());
    }
    return next.handle(req.clone({url : `${environment.API}${req.url}`})).pipe(
      catchError(this.errorHandler.handleError())
    );
  }


}
