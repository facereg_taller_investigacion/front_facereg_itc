export interface UserTecnm {
    id: number;
    numControl: number;
    nombre: string;
    apellidos: string;
    email: string;
    snVisitante: boolean;
    created_at: string;    
}
  