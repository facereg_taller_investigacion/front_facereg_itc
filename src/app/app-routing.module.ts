import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component'
import { ReconocerUsuarioComponent } from './pages/reconocer-usuario/reconocer-usuario.component';
import { RegistroRostrosComponent } from './pages/registro-rostros/registro-rostros.component';
import { RegistroUsuarioComponent } from './pages/registro-usuario/registro-usuario.component';



const routes: Routes = [
  {path: '', redirectTo: 'tecnm', pathMatch: 'full'},
  {
    path: 'tecnm',
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'registro',        
        children:[
          {
            path: ':id',
            component: RegistroRostrosComponent,
          },
          {path: '', component: RegistroUsuarioComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'reconocimiento',
        component: ReconocerUsuarioComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
