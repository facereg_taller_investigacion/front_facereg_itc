import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { WebcamImage } from 'src/app/domain/webcam/webcam-image';
import { WebcamInitError } from 'src/app/domain/webcam/webcam-init-error';
import { WebcamUtil } from 'src/app/utils/webcam.util';

@Component({
  selector: 'app-webcam-facereg',
  templateUrl: './webcam-facereg.component.html',
  styleUrls: ['./webcam-facereg.component.css']
})
export class WebcamFaceregComponent implements OnInit {
  
  @Input() public width: number = 640;  
  @Input() public height: number = 480;
  @Output() captureImage = new EventEmitter<WebcamImage>();  

   // toggle webcam on/off
   public showWebcam = true;
   public mirrorImage: string = 'always';
   public allowCameraSwitch = true;
   public multipleWebcamsAvailable = false;
   public deviceId: string;   
   public errors: WebcamInitError[] = [];
 
   // latest snapshot
   public webcamImage: WebcamImage = null;
 
   // webcam snapshot trigger
   @Input() private trigger: Subject<void> = new Subject<void>();
   // switch to next / previous / specific webcam; true/false: forward/backwards, string: deviceId
   private nextWebcam: Subject<boolean|string> = new Subject<boolean|string>();

  constructor() { }

  public ngOnInit(): void {
    WebcamUtil.getAvailableVideoInputs()
      .then((mediaDevices: MediaDeviceInfo[]) => {
        this.multipleWebcamsAvailable = mediaDevices && mediaDevices.length > 1;
      });
  }

  // public triggerSnapshot(): void {
  //   this.trigger.next();
  // }

  public toggleWebcam(): void {
    this.showWebcam = !this.showWebcam;
  }

  public handleInitError(error: WebcamInitError): void {
    this.errors.push(error);
    this.showWebcam = false;
  }

  public showNextWebcam(directionOrDeviceId: boolean|string): void {    
    this.nextWebcam.next(directionOrDeviceId);
  }

  public handleImage(webcamImage: WebcamImage): void {    
   //  console.info('received webcam image', webcamImage.imageAsBase64);
    this.webcamImage = webcamImage;
    this.captureImage.emit(webcamImage)
  }

  public cameraWasSwitched(deviceId: string): void {
    console.log('active device: ' + deviceId);
    this.deviceId = deviceId;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }

  public get nextWebcamObservable(): Observable<boolean|string> {
    return this.nextWebcam.asObservable();
  }

}
