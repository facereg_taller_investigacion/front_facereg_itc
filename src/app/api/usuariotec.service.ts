import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsuariotecService{

  constructor(public http: HttpClient) {     
  }

  public registrarUsuario(body: any): Observable<any> {
    return this.http.post('usuarios', body);
  }

  public registrarRostro(body: any): Observable<any> {
    return this.http.put('registrar_rostro', body);
  }

  public reconocimientoUsuario(body): Observable<any> {
    return this.http.post(`reconocimiento_usuario`, body);
  }

  public obtenerUsuarioPorID(id): Observable<any> {
    return this.http.get(`usuarios/${id}`);
  }

  public obtenerUsuarioPorNumControl(numControl): Observable<any> {
    return this.http.get(`usuarios/numcontrol/${numControl}`);
  }
}
